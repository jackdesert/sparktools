"""
This module patches pyspark.sql.dataframe.DataFrame so that it has an `html` method.

The `html` method is useful for preview wide data frames in a web browser.
"""
import pdb
import os

from pyspark.sql.dataframe import DataFrame

# Note importing simple_server starts an http.server
from simple_server import PORT


DIR = '/tmp/simple_server'
DEFAULT_FNAME = f'{DIR}/index.html'

os.makedirs(DIR, exist_ok=True)

class Markup:

    UTF8 = 'utf-8'

    __slots__ = ('df', 'n', 'fname')

    def __init__(self, df, n, fname):
        """
        df: (Dataframe) the dataframe to display
        n: (int) Num rows to display. (if n is None, display all rows)
        fname: (str) Where to write output
        """
        self.df = df
        self.n = n
        self.fname = fname

    def html(self):
        lines = []
        lines.append(self.style)
        lines.append('<table>')
        lines.append(self._header_html)

        # limit().collect() is the same as head()
        # See https://stackoverflow.com/questions/58263304/
        df_to_use = self.df if self.n is None else self.df.limit(self.n)
        for row_index, row in enumerate(df_to_use.collect()):
            klass = 'odd' if row_index % 2 else 'even'
            line = f'  <tr class="{klass}">\n'
            for cell in row:
                line += f'    <td>{cell}</td>\n'
            line += '  </tr>'
            lines.append(line)
        lines.append('</table>')
        return '\n'.join(lines)

    @property
    def style(self):
        return '''
        <style>
            html {
                font-family: arial;
            }
            table {
                border-collapse: collapse;
            }
            th, td {
                text-align: right;
                padding: 0 0.5rem;
            }
            tr.even{
                background: #eee;
            }
            span.thin{
                font-weight: 300;
            }
        </style>
        '''

    @property
    def _header_html(self):
        lines = []
        for struct in self.df.schema:
            colname = struct.name
            coltype = str(struct.dataType).replace('Type', '')
            line = f'    <th>{colname} <span class="thin">({coltype})</span></th>'
            lines.append(line)
        text = '\n'.join(lines)
        output = f'  <tr>\n{text}\n  </tr>'
        return output

    def write(self):
        with open(self.fname, 'w', encoding=self.UTF8) as handle:
            handle.write(self.html())
        available = f'available at localhost:{PORT}'
        if self.n is None:
            print(f'Full DataFrame {available}')
        else:
            print(f'First {self.n} rows of DataFrame {available}')

    def open_in_browser(self):
        """
        Opens the file in a browser
        TODO: Disable this when in docker
        """
        command = f'echo {self.fname} | xargs open'
        os.system(command)


def html(df, n=None, fname=DEFAULT_FNAME):
    """
    Write dataframe to disk as an html table for easy previewing,
    and open `fname` in the default web browser.

    This is useful for previewing data of wide tables.

    :param: df :DataFrame: the data frame to display
    :param: n :int: Number of rows to display. (If n is None, displays all rows)
    :param: fname :str: Where on disk to write the html file

    >>> df.html()
    DataFrame written to /tmp/index.html
    >>> df.html(2)
    DataFrame written to /tmp/index.html
    >>> df.html(fname='/tmp/other.html')
    DataFrame written to /tmp/other.html
    >>> df.ht
    DataFrame written to /tmp/index.html
    """
    markup = Markup(df, n=n, fname=fname)
    markup.write()
    markup.open_in_browser()


# Monkey Patch DataFrame to have an html method
DataFrame.html = html

if __name__ == '__main__':

    from pyspark.sql import SparkSession
    from time import sleep


    data_ = [
        ['Person', 'Instrument'],
        ['Mike', 'Trombone'],
        ['Jack', 'Voice'],
    ]

    spark_ = SparkSession.builder.appName("Testing Html").getOrCreate()

    df_ = spark_.createDataFrame(data_)
    df_.html()
    print('Sleeping for 10 seconds while you `curl localhost:8000`')
    sleep(10)
