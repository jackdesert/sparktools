"""
Import this module in order to serve
"""

import http.server
import os
import socketserver
from threading import Thread

PORT = 8000
DIR = '/tmp/simple_server'

handler = http.server.SimpleHTTPRequestHandler

def serve_in_thread():
    """
    In a thread:
        - Change directory to DIR
        - Do the equivalent of `python -m http.server`
    """
    os.chdir(DIR)
    with socketserver.TCPServer(("", PORT), handler) as httpd:
        print("Server started at localhost:" + str(PORT))
        httpd.serve_forever()

thread = Thread(target=serve_in_thread, daemon=True)
thread.start()
