SparkTools
==========


Patches pyspark.sql.dataframe.DataFrame so that it has an `html` method

The `html` method is useful for preview wide data frames in a web browser.


Installation
------------

When you install pyspark, make sure you install the same version
to match your spark install

    pip install pyspark==<correct_version>
    pip install git+https://gitlab.com/jackdesert/sparktools


df.html()
---------

    >>>help(df.html)

    Help on method html in module sparktools.html_preview:

    html(n=None, fname='/tmp/index.html') method of pyspark.sql.dataframe.DataFrame instance
        Write dataframe to disk as an html table for easy previewing,
        and open `fname` in the default web browser.

        This is useful for previewing data of wide tables.

        :param: df :DataFrame: the data frame to display
        :param: n :int: Number of rows to display. (If n is None, displays all rows)
        :param: fname :str: Where on disk to write the html file

        >>> df.html()
        DataFrame written to /tmp/index.html
        >>> df.html(2)
        DataFrame written to /tmp/index.html
        >>> df.html(fname='/tmp/other.html')
        DataFrame written to /tmp/other.html
        >>> df.ht
        DataFrame written to /tmp/index.html



Example
------------

First, import sparktools for side-effects
(One of the side effects is that it starts an http.server)


    from pyspark.sql import SparkSession
    import sparktools # imported for side-effects

    data = [['Person', 'Instrument'],
             ['Mike', 'Trombone'],
             ['Jack', 'Voice'],
             ['Jennifer', 'Timpani'],
             ['Lisa', 'Guitar'],
             ]


    spark = (SparkSession
             .builder
             .appName("Demonstrate HTML Preview")
             .getOrCreate())

    df = spark.createDataFrame(data)

    # Generate html with all rows
    df.html()

    # Generate html with first two rows
    df.html(2)


Screenshots
-----------

![A Wide Table](images/landscape.png "A Wide Table")

