from setuptools import setup

setup(
    name='sparktools',
    version='0.3.0',
    author='Jack Desert',
    url='http://gitlab.com/jackdesert/sparktools',
    license='MIT',
    description='Development tools for working with pyspark',
    packages=['sparktools'],
    install_requires=[
        # Make sure you install the version of pyspark to match
        # your version of spark (installed at the OS level)
        "pyspark",
    ],
)
